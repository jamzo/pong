using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Citra.Pong.Entities;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using Citra.Pong.Assets;

namespace Citra.Pong.Views
{
	public class Board : IView
	{
		public enum Side { Left, Right };

		public EntityManager Entities;

		public int Width = Game1.Width;
		public int Height = Game1.Height;

		public Player Player1, Player2;

		public int GutterSize;

		public Ball Ball;

		float containerSpawnChance = 0.0001f;

		public Board ()
		{
			GutterSize = Game1.SquareSize;

			Entities = new EntityManager (this);

			Ball = new Ball(this);
			Entities.Add (Ball);

			Paddle paddle = new Paddle (this);
			Entities.Add (paddle);

			Player1 = new Player (Player.Types.Human, Side.Left, paddle, this);

			paddle = new Paddle (this);
			Entities.Add (paddle);

			Player2 = new Player (Player.Types.Human, Side.Right, paddle, this);
		}

		public void Update()
		{
			Player1.Update ();
			Player2.Update ();
			Entities.Update ();

			Random rand = new Random ();
			if (rand.Next (1000) < containerSpawnChance * 1000) {
				int padding = GutterSize * 3;
				Rectangle containerArea = new Rectangle (padding, padding, Width - padding * 2, Height - padding * 2);
				Entities.Add (new Container (this) { 
					Position = new Vector2 (rand.Next (containerArea.Width) + containerArea.X,
						rand.Next (containerArea.Height) + containerArea.Y)
				});
			}
		}

		public void Draw(SpriteBatch batch)
		{
			DrawBoard (batch);
			DrawScore (batch);
			DrawEntities (batch);
		}


		void DrawBoard (SpriteBatch batch)
		{
			batch.Draw (AssetManager.Graphics ["pixel"], new Rectangle (0, 0, Width, Game1.SquareSize), Color.White);
			batch.Draw (AssetManager.Graphics ["pixel"], new Rectangle (0, Height - Game1.SquareSize, Width, Game1.SquareSize), Color.White);
			for (int y = 0; y < BottomEdge; y += Game1.SquareSize * 2) {
				batch.Draw (AssetManager.Graphics ["pixel"], new Rectangle (Width / 2 - Game1.SquareSize / 2, y, Game1.SquareSize, Game1.SquareSize), Color.White);
			}
		}

		void DrawScore (SpriteBatch batch)
		{
			int scoreSize = 64;
			Spritefont.DrawString (batch, Player1.Points.ToString (), new Vector2 (scoreSize, scoreSize), scoreSize, Color.White);
			Spritefont.DrawString (batch, Player2.Points.ToString (), new Vector2 (Width - scoreSize - Player2.Points.ToString ().Length * (float)scoreSize, (float)scoreSize), scoreSize, Color.White);
		}

		void DrawEntities (SpriteBatch batch)
		{
			foreach (Entity entity in Entities)
				batch.Draw (entity.Texture, entity.Box, entity.Color);
		}

		public void Score(Side side)
		{
			if (side == Side.Left)
				Player1.Points++;
			else if (side == Side.Right)
				Player2.Points++;
		}

		public int TopEdge
		{
			get {
				return 0 + GutterSize;
			}
		}

		public int BottomEdge {
			get {
				return Height - GutterSize;
			}
		}
	}
}  