using System;
using Microsoft.Xna.Framework.Graphics;

namespace Citra.Pong.Views
{
	public interface IView
	{
		void Update();
		void Draw (SpriteBatch batch);
	}
}

