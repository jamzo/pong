using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace Citra.Pong
{
    public static class Input
    {
        public static KeyboardState[] Keyboard = new KeyboardState[2];

		public static bool Changed = Keyboard [0] != Keyboard [1];

        public static void Update(KeyboardState kState)
        {
            Keyboard[0] = Keyboard[1];
            Keyboard[1] = kState;
        }

		public static List<Keys> GetDown()
		{
			return Keyboard[1].GetPressedKeys().ToList();
		}

        public static Boolean Down(Keys key)
        {
            return Keyboard[1].IsKeyDown(key);
        }

        public static Boolean Pressed(Keys key)
        {

            return !Keyboard[0].IsKeyDown(key) && Keyboard[1].IsKeyDown(key);
        }
        public static Boolean Released(Keys key)
        {
            return Keyboard[0].IsKeyDown(key) && !Keyboard[1].IsKeyDown(key);
		}
    }
}
