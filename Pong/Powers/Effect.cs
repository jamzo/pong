using System;

namespace Citra.Pong.Powers
{
	public class Effect
	{
		public enum Types { PaddleSize, PaddleSpeed, BallSize, BallSpeed }
		public Types Type;
		public double Multiplier;

		public Effect (Types type, double multiplier)
		{
			Type = type;
			Multiplier = multiplier;
		}
	}
}

