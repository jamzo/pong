using System;

namespace Citra.Pong.Powers
{
	public class Power
	{
		public static Power LargeSlow = new Power() {
			Effects = new Effect[] { new Effect(Effect.Types.PaddleSize, 1.5), new Effect(Effect.Types.PaddleSpeed, 0.5) }
		};

		public static Power SmallFast = new Power() {
			Effects = new Effect[] { new Effect(Effect.Types.PaddleSize, 0.75), new Effect(Effect.Types.PaddleSpeed, 1.5) }
		};

		public Effect[] Effects = new Effect[] {};
		public int Duration = 600;
	}
}

