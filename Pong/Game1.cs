#region File Description
//-----------------------------------------------------------------------------
// PongGame.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
using Citra.Pong.Entities;
using Citra.Pong.Assets;


#endregion

#region Using Statements
using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Input.Touch;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Media;
using Citra.Pong.Views;

#endregion
namespace Citra.Pong
{
	/// <summary>
	/// Default Project Template
	/// </summary>
	public class Game1 : Game
	{

		#region Fields

		GraphicsDeviceManager graphics;
		SpriteBatch spriteBatch;
		bool pause;
		RenderTarget2D screen;

		public static Config Config;
		public static int Width, Height;
		public static int SquareSize;

		public static IView View;

		#endregion

		#region Initialization

		public Game1 ()
		{
			LoadConfig ();

			graphics = new GraphicsDeviceManager (this);
			graphics.PreferredBackBufferWidth = Width;
			graphics.PreferredBackBufferHeight = Height;
			
			Content.RootDirectory = "Content";

			graphics.IsFullScreen = false;
		}

		/// <summary>
		/// Overridden from the base Game.Initialize. Once the GraphicsDevice is setup,
		/// we'll use the viewport to initialize some values.
		/// </summary>
		protected override void Initialize ()
		{
			base.Initialize ();

			Input.Update (Keyboard.GetState ());
			View = new Board ();

			pause = false;
		}

		protected void LoadConfig()
		{
			Config = new Config ("config.ini");
			Width = Int32.Parse(Config ["width"]);
			Height = Int32.Parse(Config ["height"]);
			SquareSize = Int32.Parse(Config ["squareSize"]);
		}

		/// <summary>
		/// Load your graphics content.
		/// </summary>
		protected override void LoadContent ()
		{
			// Create a new SpriteBatch, which can be use to draw textures.
			spriteBatch = new SpriteBatch (graphics.GraphicsDevice);
			screen = new RenderTarget2D (GraphicsDevice, Width, Height, 
				false, SurfaceFormat.Color, DepthFormat.None);

			AssetManager.Load (Content);
		}

		#endregion

		#region Update and Draw

		/// <summary>
		/// Allows the game to run logic such as updating the world,
		/// checking for collisions, gathering input, and playing audio.
		/// </summary>
		/// <param name="gameTime">Provides a snapshot of timing values.</param>
		protected override void Update (GameTime gameTime)
		{
			Input.Update (Keyboard.GetState ());

			if (Input.Released (Keys.Space))
				pause = !pause;

			if(!pause) View.Update ();		
           
			base.Update (gameTime);
		}

		/// <summary>
		/// This is called when the game should draw itself. 
		/// </summary>
		/// <param name="gameTime">Provides a snapshot of timing values.</param>
		protected override void Draw (GameTime gameTime)
		{
			DrawScreen ();
			OutputScreen ();

			base.Draw (gameTime);
		}

		void DrawScreen ()
		{
			graphics.GraphicsDevice.SetRenderTarget (screen);
			graphics.GraphicsDevice.Clear (Color.Black);

			spriteBatch.Begin (SpriteSortMode.Immediate, BlendState.AlphaBlend, SamplerState.PointClamp, null, null);
			View.Draw (spriteBatch);
			spriteBatch.End ();
		}

		void OutputScreen ()
		{
			graphics.GraphicsDevice.SetRenderTarget (null);
			graphics.GraphicsDevice.Clear (Color.Black);

			spriteBatch.Begin ();
			graphics.GraphicsDevice.BlendState = BlendState.AlphaBlend;
			spriteBatch.Draw ((Texture2D)screen, Vector2.Zero, pause ? Color.White * 0.5f : Color.White);

			if (pause) {
				int pauseSize = 32;
				spriteBatch.Draw (AssetManager.Graphics ["pixel"], new Rectangle (Width / 2 - pauseSize / 4 - pauseSize, Height / 2 - pauseSize, 
					pauseSize, pauseSize * 2), Color.White);
				spriteBatch.Draw (AssetManager.Graphics ["pixel"], new Rectangle (Width / 2 + pauseSize / 4, Height / 2 - pauseSize, 
					pauseSize, pauseSize * 2), Color.White);
			}

			spriteBatch.End ();
		}

		#endregion

	}
}
