using System;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Citra.Pong.Assets;

namespace Citra.Pong
{
	public static class Spritefont
	{
		public static void DrawString(SpriteBatch batch, string str, Vector2 position, int size, Color color)
		{

			int x = 0;

			foreach (char c in str) {
				int index;

				try {
					index = Int32.Parse(c.ToString());
				} catch { 
					index = 0;
				}

				batch.Draw (AssetManager.Graphics ["font"],
					new Rectangle ((int)position.X + x * size, (int)position.Y, size, size),
					new Rectangle (index * 16, 0, 16, 16),
					color);

				x++;
			}
		}
	}
}

