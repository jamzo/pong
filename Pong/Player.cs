using System;
using Citra.Pong.Entities;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework;
using Citra.Pong.Views;

namespace Citra.Pong
{
	public class Player
	{
		public enum Types { Human, CPU };
		public Types Type;
		public Board.Side Side;
		public int Points;

		Paddle paddle;
		Board board;

		public Player (Types type, Board.Side side, Paddle paddle, Board board)
		{
			this.paddle = paddle;
			this.board = board;

			Type = type;
			Side = side;
			Points = 0;

			if (Side == Board.Side.Left) {
				paddle.Position.X = board.GutterSize;
				paddle.Color = Color.DeepSkyBlue;
			} else if (Side == Board.Side.Right) {
				paddle.Position.X = board.Width - board.GutterSize - paddle.Width;
				paddle.Color = Color.Red;
			}

			paddle.Position.Y = board.Height / 2 - paddle.Height / 2;
		}

		public void Score()
		{
			Points++;
		}

		public void Update()
		{
			if (Type == Player.Types.CPU) {
				ProcessAI ();
			} else if (Type == Player.Types.Human) {
				ProcessHuman ();
			}
		}

		void ProcessHuman()
		{
			if (Side == Board.Side.Left) {
				ProcessInput (Keys.W, Keys.S);
			} else if (Side == Board.Side.Right) {
				ProcessInput (Keys.Up, Keys.Down);
			}
		}

		void ProcessInput(Keys upKey, Keys downKey)
		{
			if (Input.Down (upKey) || Input.Down (downKey))
				paddle.Accelerating = true;
			else
				paddle.Accelerating = false;

			if (Input.Down (upKey) && paddle.Velocity.Y >= 0) {
				paddle.Velocity.Y = -1;
			} else if (Input.Down (downKey) && paddle.Velocity.Y <= 0) {
				paddle.Velocity.Y = 1;
			}
		}

		void ProcessAI ()
		{
			if (board.Ball.Position.Y < paddle.Position.Y + paddle.Height / 2
				&& paddle.Velocity.Y >= 0) {
				paddle.Accelerating = true;
				paddle.Velocity.Y = -1;
			} else if (board.Ball.Position.Y > paddle.Position.Y + paddle.Height / 2
				&& paddle.Velocity.Y <= 0) {
				paddle.Accelerating = true;
				paddle.Velocity.Y = 1;
			}
		}
	}
}

