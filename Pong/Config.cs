using System;
using System.Collections.Generic;
using System.IO;

namespace Citra.Pong
{
	public class Config
	{
		static Dictionary<string, string> dictionary;

		public Config(string file)
		{
			Dictionary<string, string> results = new Dictionary<string, string>();
			string fileText = File.ReadAllText(file);
			string[] fileLines = fileText.Split(new char[] { '\r', '\n' });
			if (fileLines.Length > 0)
				for (int i = 0; i < fileLines.Length; i++)
				{
					string line = fileLines[i].Trim();
					if (!string.IsNullOrEmpty(line))
					{
						int equalsLocation = line.IndexOf('=');
						if (equalsLocation > 0)
						{
							string key = line.Substring(0, equalsLocation).Trim();
							string value = line.Substring(equalsLocation + 1, line.Length - equalsLocation - 1);

							results.Add(key, value);
						}
					}
				}

			dictionary = results;
		}

		public string this[string key] 
		{
			get {
				return dictionary [key];
			}
		}
	}
}

