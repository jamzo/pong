using System;
using Microsoft.Xna.Framework;
using Citra.Pong.Powers;
using Citra.Pong.Views;

namespace Citra.Pong.Entities
{
	public class Paddle : Entity
	{
		Power power;
		int powerTimer;

		public Paddle (Board board)
			: base("pixel", board)
		{
			Width = Game1.SquareSize;
			Height = Width * 4;
			maxVelocity = 5f;
			acceleration = 0.1f;
			powerTimer = 0;
			power = null;
		}

		public override void Update()
		{
			base.Update ();

			if (Position.X < 0)
				Position.X = 0;
			else if (Position.X + Width > board.Width)
				Position.X = board.Width - Width;

			if (Position.Y < board.TopEdge)
				Position.Y = board.TopEdge;
			else if (Position.Y + Height > board.BottomEdge)
				Position.Y = board.BottomEdge - Height;

			if (power != null) {
				powerTimer--;

				if (powerTimer <= 0)
					RemovePower ();
			}
		}

		public void RemovePower()
		{
			power = null;
			powerTimer = 0;
			SizeMultiplier = 1;
			SpeedMultiplier = 1;
		}

		public void SetPower(Power power)
		{
			RemovePower ();

			powerTimer = power.Duration;

			foreach (Effect effect in power.Effects) {
				switch (effect.Type) {
				case Effect.Types.PaddleSize:
					SizeMultiplier = effect.Multiplier;
					break;
				case Effect.Types.PaddleSpeed:
					SpeedMultiplier = effect.Multiplier;
					break;
				}
			}

			this.power = power;
		}

		public override void Collide(Entity entityB)
		{
			if (entityB.GetType () == typeof(Ball)) {
				Velocity /= 2;
			}
		}
	}
}

