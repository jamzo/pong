using System;
using Microsoft.Xna.Framework;
using Citra.Pong.Views;

namespace Citra.Pong.Entities
{
	public class Ball : Entity
	{
		public bool InNet;
		private Paddle lastHit;

		public Ball (Board board)
			: base("pixel", board)
		{
			Width = Game1.SquareSize;
			Height = Game1.SquareSize;
			acceleration = 0f;
			maxVelocity = 7f;
			Accelerating = true;

			InNet = false;

			Position.X = board.Width / 2 - Width / 2;
			Position.Y = board.Height / 2 - Height / 2;

			Nudge ();
		}

		public override void Update()
		{
			base.Update ();
			CheckBounce ();
			CheckNet ();
		}

		void CheckBounce ()
		{
			if (Position.Y < board.TopEdge) {
				Position.Y = board.TopEdge;
				Velocity *= new Vector2(1, -1);
			}
			else if (Position.Y + Height > board.BottomEdge) 
			{
				Position.Y = board.BottomEdge - Height;
				Velocity *= new Vector2(1, -1);
			}
		}

		void CheckNet ()
		{
			if (Position.X < 0 || Position.X > board.Width + Width)
				InNet = true;
		}

		public void Nudge()
		{
			Random rand = new Random ();

			switch (rand.Next (3)) {
			case 0:
				Velocity = new Vector2 (1, -1);
				break;
			case 1:
				Velocity = new Vector2 (1, 1);
				break;
			case 2:
				Velocity = new Vector2 (-1, 1);
				break;
			case 3:
				Velocity = new Vector2 (-1, -1);
				break;
			}

			Velocity *= 3;
		}

		public override void Collide(Entity entityB)
		{
			if (entityB.GetType () == typeof(Paddle))
				CollideWithPaddle ((Paddle)entityB);

			if (entityB.GetType () == typeof(Container))
				CollideWithContainer ((Container)entityB);
		}

		void CollideWithContainer(Container container)
		{
			if (lastHit == null)
				return;

			board.Entities.Remove (container);

			switch (container.Content) {
			case Container.Contents.Power:
				lastHit.SetPower (container.Power);
				break;
			case Container.Contents.AddBall:
				board.Entities.Add (new Ball (board));
				break;
			}
		}

		void CollideWithPaddle (Paddle paddle)
		{
			if (lastHit == paddle)
				return;
			Position -= Velocity;
			lastHit = paddle;
			Color = paddle.Color;
			Velocity *= new Vector2 (1.1f, 1.1f);
			Vector2 safePos = Position;
			float sensitivity = 0.1f;
			while (!Box.Intersects (paddle.Box)) {
				safePos = Position;
				if (Velocity.X > 0)
					Position.X += sensitivity;
				else
					if (Velocity.X < 0)
						Position.X -= sensitivity;
				if (Velocity.Y > 0)
					Position.Y += sensitivity;
				else
					if (Velocity.Y < 0)
						Position.Y -= sensitivity;
			}
			Rectangle intersection = Rectangle.Intersect (Box, paddle.Box);
			if (intersection.Width < intersection.Height) {
				Velocity *= new Vector2 (-1, 1);
			}
			else
				if (intersection.Width > intersection.Height) {
					Velocity *= new Vector2 (1, -1);
				}
				else {
					Velocity *= -1;
				}
			Velocity += paddle.Velocity / 2;
			Position = safePos;
		}
	}
}

