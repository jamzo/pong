using System;
using System.Collections.Generic;
using Citra.Pong.Views;

namespace Citra.Pong.Entities
{
	public class EntityManager
	{
		List<Entity> entities;
		List<Entity> addQ;
		List<Entity> rmQ;

		Board board;

		bool updating = false;

		public EntityManager(Board board)
		{
			this.board = board;
			entities = new List<Entity> ();
			addQ = new List<Entity> ();
			rmQ = new List<Entity> ();
		}

		public void Add(Entity entity)
		{
			if (updating)
				addQ.Add (entity);
			else
				entities.Add (entity);
		}

		public void Remove(Entity entity)
		{
			if (updating)
				rmQ.Add (entity);
			else
				entities.Remove (entity);
		}

		public void Update()
		{
			updating = true;

			foreach (Entity entity in entities) {
				entity.Update ();

				if (entity.GetType () == typeof(Ball))
				{
					if(((Ball)entity).InNet) {
						if (entity.Position.X < 0) {
							board.Player2.Score ();
						} else if (entity.Position.X + entity.Width > board.Width) {
							board.Player1.Score ();
						}

						if ((int)entities.FindAll (e => e.GetType () == typeof(Ball)).Count == 1)
							Add (new Ball (board));

						Remove (entity);
					}
				}
			}

			updating = false;

			foreach (Entity entity in addQ)
				entities.Add (entity);

			foreach (Entity entity in rmQ)
				entities.Remove (entity);

			addQ = new List<Entity> ();
			rmQ = new List<Entity> ();
		}

		public List<Entity>.Enumerator GetEnumerator()
		{
			return entities.GetEnumerator ();
		}
	}
}