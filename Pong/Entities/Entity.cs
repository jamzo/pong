using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Citra.Pong.Views;
using Citra.Pong.Assets;

namespace Citra.Pong.Entities
{
	public class Entity
	{
		public enum Edge { Top, Right, Bottom, Left };

		protected Board board;

		public String Graphic { get; set; }
		protected int width, height;
		public Vector2 Position;
		public Color Color;

		public Vector2 Velocity;
		protected float acceleration;
		public bool Accelerating;
		protected float maxVelocity;

		public double SizeMultiplier = 1, SpeedMultiplier = 1;

		public Entity(string graphic, Board board)
		{
			this.board = board;

			Width = 0;
			Height = 0;
			Position = Vector2.Zero;
			Color = Color.White;
			Graphic = graphic;

			acceleration = 0f;
			Accelerating = false;
			maxVelocity = 0f;
		}

		public virtual void Update()
		{
			Vector2 velocity = Velocity;

			if (velocity != Vector2.Zero) {
				if (velocity.X > 0) {
					if (Accelerating) {
						velocity.X += acceleration;
						if (velocity.X > maxVelocity)
							velocity.X = maxVelocity;
					} else {
						velocity.X -= acceleration;
						if (velocity.X < 0)
							velocity.X = 0;
					}
				} else if (velocity.X < 0) {
					if (Accelerating) {
						velocity.X -= acceleration;
						if (velocity.X < -maxVelocity)
							velocity.X = -maxVelocity;
					} else {
						velocity.X += acceleration;
						if (velocity.X > 0)
							velocity.X = 0;
					}
				}

				if (velocity.Y > 0) {
					if (Accelerating) {
						velocity.Y += acceleration;
						if (velocity.Y > maxVelocity)
							velocity.Y = maxVelocity;
					} else {
						velocity.Y -= acceleration;
						if (velocity.Y < 0)
							velocity.Y = 0;
					}
				} else if (Velocity.Y < 0) {
					if (Accelerating) {
						velocity.Y -= acceleration;
						if (velocity.Y < -maxVelocity)
							velocity.Y = -maxVelocity;
					} else {
						velocity.Y += acceleration;
						if (velocity.Y > 0)
							velocity.Y = 0;
					}
				}
			}

			Velocity = velocity;
			Position += Velocity * (float)SpeedMultiplier;

			foreach (Entity entity in board.Entities) {
				if (Box.Intersects (entity.Box)) {
					Collide (entity);
					entity.Collide (this);
				}
			}
		}

		public virtual void Collide(Entity entityB)
		{
		}

		public void SetMultipliers(double size, double speed)
		{
			SizeMultiplier = size;
			SpeedMultiplier = speed;
		} 

		public Texture2D Texture
		{
			get {
				return AssetManager.Graphics [Graphic];
			}
		}

		public Rectangle Box 
		{
			get {
				return new Rectangle ((int)Position.X, (int)Position.Y, Width, Height);
			}
		}

		public Rectangle GetEdge(Edge edge)
		{
			switch (edge) {
			case Edge.Top:
				return new Rectangle (Box.Left, Box.Top, Box.Width, 1);
			case Edge.Right:
				return new Rectangle (Box.Right, Box.Top, 1, Box.Height);
			case Edge.Bottom:
				return new Rectangle (Box.Left, Box.Bottom, Box.Width, 1);
			case Edge.Left:
				return new Rectangle (Box.Left, Box.Top, 1, Box.Height);
			default:
				return Rectangle.Empty;
			}
		}

		public int Width {
			get { 
				return width;
			}

			set {
				width = value;
			}
		}

		public int Height {
			get {
				return (int)((double)height * SizeMultiplier);
			}

			set {
				height = value;
			}
		}
	}
}

