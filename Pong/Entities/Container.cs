using System;
using Microsoft.Xna.Framework;
using System.Collections.Generic;
using Citra.Pong.Powers;
using Citra.Pong.Views;

namespace Citra.Pong.Entities
{
	public class Container : Entity
	{
		public enum Contents { Power, AddBall };
		public Contents Content;
		public Power Power;
		int timeLeft;

		public Container (Board board)
			: base ("container", board)
		{
			Width = (int)((double)Game1.SquareSize * 1.75);
			Height = (int)((double)Game1.SquareSize * 1.75);
			Color = Color.GreenYellow;
			timeLeft = 60 * 10;
			ChooseEffect ();
		}

		public override void Update()
		{
			base.Update ();

			timeLeft--;

			if (timeLeft < 1)
				board.Entities.Remove (this);
		}

		public void ChooseEffect()
		{
			List<Contents> contents = new List<Contents> () { Contents.Power, Contents.AddBall };
			List<Power> powers = new List<Power> () { Power.LargeSlow, Power.SmallFast };

			Random rand = new Random ();

			Content = contents[rand.Next (contents.Count)];

			if (Content == Contents.Power) {
				Power = powers [rand.Next (powers.Count)];
			}
		}
	}
}

