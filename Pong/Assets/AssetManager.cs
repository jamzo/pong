using System;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace Citra.Pong.Assets
{
	public static class AssetManager
	{
		public static AssetGroup<Texture2D> Graphics = new AssetGroup<Texture2D>();
		public static AssetGroup<SpriteFont> Fonts = new AssetGroup<SpriteFont>();

		public static void Load(ContentManager content)
		{
			Graphics.Load (content, "pixel");
			Graphics.Load (content, "font");
			Graphics.Load (content, "container");
		}
	}
}

