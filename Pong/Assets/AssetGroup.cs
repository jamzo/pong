using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace Citra.Pong.Assets
{
	public class AssetGroup<T>
	{
		private Dictionary<string, T> data = new Dictionary<string, T> ();

		public T this[string key]
		{
			get
			{
				try {
					return data[key];
				} catch {
					throw new Exception(String.Format("Texture {0} not found.", key));
				}
			}

			set {
				this [key] = value;
			}
		}

		public void Load(ContentManager content, string asset)
		{
			data [asset] = content.Load<T> (asset);
		}
	}
}

